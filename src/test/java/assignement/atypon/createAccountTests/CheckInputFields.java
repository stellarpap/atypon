package assignement.atypon.createAccountTests;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.persado.oss.quality.stevia.selenium.core.SteviaContext;

import assignement.atypon.common.pageObjects.CreateAccountPageObject;
import assignement.atypon.common.pageObjects.LoginPageObject;
import assignement.atypon.common.pageObjects.MainPageObject;
import assignement.atypon.context.TestBase;

public class CheckInputFields extends TestBase {

	private static final long serialVersionUID = 6329527528561712167L;

	LoginPageObject loginPage;
	@Autowired
	MainPageObject mainPage;
	CreateAccountPageObject createAccount;
	String username = RandomStringUtils.randomAlphanumeric(6);
	String password = RandomStringUtils.randomAlphanumeric(10);
	String email = RandomStringUtils.randomAlphanumeric(5).concat("@example.com");
	String captcha = RandomStringUtils.randomAlphanumeric(8);
	
	@BeforeClass
	public void navigateToLoginPage() {
		mainPage.controller().navigate(SteviaContext.getParam("targetHostUrl"));
		createAccount = mainPage.pressLinkCreateAccount();
	}
	
	@Test(alwaysRun = true, description = "provide username and check text is inserted correctly in field")
	public void testStep_1() {
		createAccount.inputUsername(username);
		createAccount.checkUsernameText(username);
	}
	

	@Test(alwaysRun = true, dependsOnMethods = "testStep_1", description = "provide password and check text is inserted correctly in field")
	public void testStep_2() {
		createAccount.inputPassword(password);
		createAccount.checkPasswordText(password);
	}
	
	@Test(alwaysRun = true, dependsOnMethods = "testStep_2", description = "provide password confirmation and check text is inserted correctly in field")
	public void testStep_3() {
		createAccount.inputConfirmPassword(password);
		createAccount.checkConfirmPasswordText(password);
	}
	
	@Test(alwaysRun = true, dependsOnMethods = "testStep_3", description = "provide email and check text is inserted correctly in field")
	public void testStep_4() {
		createAccount.inputEmail(email);
		createAccount.checkEmailText(email);
	}
	
	@Test(alwaysRun = true, dependsOnMethods = "testStep_4", description = "provide captcha and check text is inserted correctly in field")
	public void testStep_5() {
		createAccount.inputCaptcha(captcha);
		createAccount.checkCaptchText(captcha);
	}
	
	@Test(alwaysRun = true, dependsOnMethods = "testStep_5", description = "check submit button is visible")
	public void testStep_6() {
		createAccount.checkSubmitButtonVisible();
	}

}
