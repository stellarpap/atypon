package assignement.atypon.createAccountTests;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.persado.oss.quality.stevia.selenium.core.SteviaContext;

import assignement.atypon.common.pageObjects.CreateAccountPageObject;
import assignement.atypon.common.pageObjects.LoginPageObject;
import assignement.atypon.common.pageObjects.MainPageObject;
import assignement.atypon.context.TestBase;

public class ValidationErrors extends TestBase {

	private static final long serialVersionUID = 6329527528561712167L;

	LoginPageObject loginPage;
	@Autowired
	MainPageObject mainPage;
	CreateAccountPageObject createAccount;
	String username = RandomStringUtils.randomAlphanumeric(15);
	String password = RandomStringUtils.randomAlphanumeric(10);
	String email = RandomStringUtils.randomAlphanumeric(5).concat("@example.com");
	String captcha = RandomStringUtils.randomAlphanumeric(8);
	String requiredField = "Please fill out this field.";
	
	@BeforeClass
	public void navigateToLoginPage() {
		mainPage.controller().navigate(SteviaContext.getParam("targetHostUrl"));
		createAccount = mainPage.pressLinkCreateAccount();
	}
	
	@Test(alwaysRun = true, description = "provide username that exists, and check error")
	public void testStep_1() {
		createAccount.inputUsername(SteviaContext.getParam("username"));
		createAccount.checkErrorUsernameExistsVisible();
	}
	

	@Test(alwaysRun = true, dependsOnMethods = "testStep_1", description = "complete all fields but put a wrong captcha, check error")
	public void testStep_2() {
		createAccount.inputUsername(username);
		createAccount.inputPassword(password);
		createAccount.inputConfirmPassword(password);
		createAccount.inputEmail(email);
		createAccount.inputCaptcha(captcha);
		createAccount.pressButtonSubmit();
		createAccount.checkCaptchaErrorVisible();
	}
	
	@Test(alwaysRun = true, dependsOnMethods = "testStep_2", description = "complete nothing check browser validation message in username input")
	public void testStep_3() throws Exception {
		createAccount.cleanAllInputFields();
		createAccount.pressButtonSubmit();
		SteviaContext.verify().equal(createAccount.getValidationTextInputFields("username"), requiredField);
	}
	
	@Test(alwaysRun = true, dependsOnMethods = "testStep_3", description = "complete username, but not password, check browser validation for password")
	public void testStep_4() throws Exception {
		createAccount.cleanAllInputFields();
		createAccount.inputUsername(username);
		createAccount.pressButtonSubmit();
		SteviaContext.verify().equal(createAccount.getValidationTextInputFields("password"), requiredField);
	}
	
	@Test(alwaysRun = true, dependsOnMethods = "testStep_4", description = "complete username,password but not password confirm, check browser validation for password confirm")
	public void testStep_5() throws Exception {
		createAccount.cleanAllInputFields();
		createAccount.inputUsername(username);
		createAccount.inputPassword(password);
		createAccount.pressButtonSubmit();
		SteviaContext.verify().equal(createAccount.getValidationTextInputFields("confirmpassword"), requiredField);
	}
	
	@Test(alwaysRun = true, dependsOnMethods = "testStep_5", description = "complete username,password, confirm password but not captcha, check browser validation for captcha")
	public void testStep_6() throws Exception {
		createAccount.cleanAllInputFields();
		createAccount.inputUsername(username);
		createAccount.inputPassword(password);
		createAccount.inputConfirmPassword(password);
		createAccount.pressButtonSubmit();
		SteviaContext.verify().equal(createAccount.getValidationTextInputFields("captcha"), requiredField);
	}
}
