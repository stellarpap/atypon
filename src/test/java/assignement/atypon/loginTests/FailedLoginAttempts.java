package assignement.atypon.loginTests;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.persado.oss.quality.stevia.selenium.core.SteviaContext;

import assignement.atypon.common.pageObjects.LoginPageObject;
import assignement.atypon.common.pageObjects.MainPageObject;
import assignement.atypon.context.TestBase;

public class FailedLoginAttempts extends TestBase {

	private static final long serialVersionUID = 6329527528561712167L;

	LoginPageObject loginPage;
	@Autowired
	MainPageObject mainPage;
	
	String requiredField = "Please fill out this field.";
	
	
	@BeforeClass
	public void navigateToLoginPage() {
		mainPage.controller().navigate(SteviaContext.getParam("targetHostUrl"));
		loginPage = mainPage.pressLinkLogin();
		loginPage.checkLoginHeaderVisible();
	}
	
	@Test(alwaysRun = true, description = "provide valid username, invalid password, check validation error exists")
	public void testStep_1() {
		loginPage.inputUsername(SteviaContext.getParam("username"));
		loginPage.inputPassword(RandomStringUtils.randomAlphanumeric(10));
		loginPage.pressButtonLogin();
		loginPage.checkErrorInLoginFormVisible();
	}

	@Test(alwaysRun = true, dependsOnMethods = "testStep_1", description = "provide valid password, invalid username, check validation error exists")
	public void testStep_2() {
		loginPage.clearUsernamePassword();
		loginPage.inputUsername(RandomStringUtils.randomAlphanumeric(10));
		loginPage.inputPassword(SteviaContext.getParam("password"));
		loginPage.pressButtonLogin();
		loginPage.checkErrorInLoginFormVisible();
	}
	
	@Test(alwaysRun = true, dependsOnMethods = "testStep_2", description = "provide invalid username and password, check validation error exists")
	public void testStep_3() {
		loginPage.clearUsernamePassword();
		loginPage.inputUsername(RandomStringUtils.randomAlphanumeric(12));
		loginPage.inputPassword(RandomStringUtils.randomAlphanumeric(16));
		loginPage.pressButtonLogin();
		loginPage.checkErrorInLoginFormVisible();
	}
	
	@Test(alwaysRun = true, dependsOnMethods = "testStep_3", description = "provide username and blank password, check missing input error message exists")
	public void testStep_4() throws Exception {
		loginPage.clearUsernamePassword();
		loginPage.inputUsername(RandomStringUtils.randomAlphanumeric(6));
		loginPage.pressButtonLogin();
		SteviaContext.verify().equal(loginPage.getValidationTextInputFields("password"), requiredField);
	}
	
	@Test(alwaysRun = true, dependsOnMethods = "testStep_4", description = "provide password and blank username, check missing input error message exists")
	public void testStep_5() throws Exception {
		loginPage.clearUsernamePassword();
		loginPage.inputPassword(RandomStringUtils.randomAlphanumeric(11));
		loginPage.pressButtonLogin();
		SteviaContext.verify().equal(loginPage.getValidationTextInputFields("username"), requiredField);
	}
	
	@Test(alwaysRun = true, dependsOnMethods = "testStep_5", description = "provide blank password and blank username, check missing input error message exists")
	public void testStep_6() throws Exception {
		loginPage.clearUsernamePassword();
		loginPage.pressButtonLogin();
		SteviaContext.verify().equal(loginPage.getValidationTextInputFields("username"), requiredField);
	}
	
	@AfterClass(alwaysRun = true)
	public void logOut() {
		mainPage.pressLinkLogOut();
	}
	

}
