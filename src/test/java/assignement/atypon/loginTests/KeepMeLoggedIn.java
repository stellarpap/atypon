package assignement.atypon.loginTests;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.persado.oss.quality.stevia.selenium.core.SteviaContext;
import assignement.atypon.common.pageObjects.LoginPageObject;
import assignement.atypon.common.pageObjects.MainPageObject;
import assignement.atypon.common.utilities.GeneralMethods;
import assignement.atypon.context.TestBase;

public class KeepMeLoggedIn extends TestBase {

	private static final long serialVersionUID = 6329527528561712167L;

	LoginPageObject loginPage;
	@Autowired
	MainPageObject mainPage;
	String username;
	String password; 
	GeneralMethods genMethods = new GeneralMethods();
	String fileName = "userAccounts.csv";
	
	@BeforeClass
	public void createInitialData() throws FileNotFoundException, IOException {
		mainPage.controller().navigate(SteviaContext.getParam("targetHostUrl"));
		String creds = genMethods.getRandomUserPassPair(fileName, ",");
		String[] userPass = creds.split(",");
		username = userPass[0];
		password = userPass[1];
		loginPage = mainPage.pressLinkLogin();
		loginPage.checkLoginHeaderVisible();
	}
	
	@Test(alwaysRun = true, description = " Login, press keep me logged in and verify if the checkbox is checked")
	public void testStep_1() {
		loginPage.inputUsername(username);
		loginPage.inputPassword(password);
		loginPage.pressKeepMeLoggedIn();
		SteviaContext.verify().equal(loginPage.isKeepMeLoggedInIsChecked(), true);
	}

	@Test(alwaysRun = true, dependsOnMethods = "testStep_1", description = "press submit button, and check you are logged in")
	public void testStep_2() {
		loginPage.pressButtonLogin();
		mainPage.checLoggedInUsername(username);
	}
	
	@Test(alwaysRun = true, dependsOnMethods = "testStep_2", description = "navigate to url outside mediawiki, return and check you're still logged in")
	public void testStep_3() {
		mainPage.controller().navigate("https://www.atypon.com/");
		mainPage.controller().navigate(SteviaContext.getParam("targetHostUrl"));
		mainPage.checLoggedInUsername(username);
	}
	
	@AfterClass(alwaysRun = true)
	public void logOut() {
		mainPage.pressLinkLogOut();
	}
	

}
