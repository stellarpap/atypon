package assignement.atypon.loginTests;

import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.persado.oss.quality.stevia.selenium.core.SteviaContext;
import assignement.atypon.common.pageObjects.LoginPageObject;
import assignement.atypon.common.pageObjects.MainPageObject;
import assignement.atypon.context.TestBase;

public class SuccessfulLoginTest extends TestBase {

	private static final long serialVersionUID = 6329527528561712167L;

	LoginPageObject loginPage;
	@Autowired
	MainPageObject mainPage;
	String username;
	String password; 
	
	@BeforeClass
	public void createInitialData() {
		mainPage.controller().navigate(SteviaContext.getParam("targetHostUrl"));
		username = SteviaContext.getParam("username");
		password = SteviaContext.getParam("password");
		mainPage.checkLogoIsVisible();
	}
	
	@Test(alwaysRun = true, description = " press link login in, check that user is in login page")
	public void testStep_1() {
		loginPage = mainPage.pressLinkLogin();
		loginPage.checkLoginHeaderVisible();
	}

	@Test(alwaysRun = true, dependsOnMethods = "testStep_1", description = "Input username  check username value in respective field")
	public void testStep_2() {
		loginPage.inputUsername(username);
		loginPage.checkUsernameInput(username);
	}
	
	@Test(alwaysRun = true, dependsOnMethods = "testStep_2", description = "input password and check password value in respective field")
	public void testStep_3() {
		loginPage.inputPassword(password);
		loginPage.checkPasswordInput(password);
	}
	
	@Test(alwaysRun = true, dependsOnMethods = "testStep_3", description = "press login button, and check logged in username")
	public void testStep_4() {
		loginPage.pressButtonLogin();
		mainPage.checLoggedInUsername(username);
	}
	
	@Test(alwaysRun = true, dependsOnMethods = "testStep_4", description = "check log out link visible")
	public void testStep_5() {
		mainPage.checkLogOutLinkVisible();
	}
	
	@AfterClass(alwaysRun = true)
	public void logOut() {
		mainPage.pressLinkLogOut();
	}
	

}
