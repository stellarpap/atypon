package assignement.atypon.loginTests;

import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.persado.oss.quality.stevia.selenium.core.SteviaContext;
import assignement.atypon.common.pageObjects.LoginPageObject;
import assignement.atypon.common.pageObjects.MainPageObject;
import assignement.atypon.common.pageObjects.ResetPasswordPageObject;
import assignement.atypon.context.TestBase;

public class LoginComplementaryOptions extends TestBase {

	private static final long serialVersionUID = 6329527528561712167L;

	LoginPageObject loginPage;
	@Autowired
	MainPageObject mainPage;
	ResetPasswordPageObject resetPassword;
	String username;
	String password; 
	
	@BeforeClass
	public void navigateToLinkLogin() {
		mainPage.controller().navigate(SteviaContext.getParam("targetHostUrl"));
		loginPage = mainPage.pressLinkLogin();
		loginPage.checkLoginHeaderVisible();
	}
	
	@Test(alwaysRun = true, description = " press link help with login in, check that user is in the appropriate page, via url check")
	public void testStep_1() {
		loginPage.pressLinkHelpLogin();
		SteviaContext.verify().equal(loginPage.controller().getCurrentUrl().contains("Help:Logging_in"), true);
	}

	@Test(alwaysRun = true, dependsOnMethods = "testStep_1", description = "navigate to login again, press forget password link, check input for new username is visible")
	public void testStep_2() {	
		mainPage.pressLinkLogin();
		resetPassword = loginPage.pressLinkForgetYourPassword();
		resetPassword.checkUsernameFieldIsVisible();
	}
	
	@Test(alwaysRun = true, dependsOnMethods = "testStep_2", description = "check password input field is visible")
	public void testStep_3() {
		resetPassword.checkPasswordFieldIsVisible();
	}
	
	@Test(alwaysRun = true, dependsOnMethods = "testStep_3", description = "check reset password button is visible")
	public void testStep_4() {
		resetPassword.checkButtonResetPasswordIsVisible();
	}
	
	@AfterClass(alwaysRun = true)
	public void logOut() {
		mainPage.pressLinkLogOut();
	}
	

}
