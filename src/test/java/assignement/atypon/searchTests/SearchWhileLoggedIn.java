package assignement.atypon.searchTests;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.persado.oss.quality.stevia.selenium.core.SteviaContext;

import assignement.atypon.common.pageObjects.LoginPageObject;
import assignement.atypon.common.pageObjects.MainPageObject;
import assignement.atypon.common.utilities.GeneralMethods;
import assignement.atypon.context.TestBase;

public class SearchWhileLoggedIn extends TestBase {

	private static final long serialVersionUID = 1L;
@Autowired
MainPageObject mainPage;
LoginPageObject loginPage;
String search1 = "Developer hub";
String username;
String password;
GeneralMethods genMethods = new GeneralMethods();
String fileName = "userAccounts.csv";
	
	@BeforeClass
	public void navigateToLoginPage() throws FileNotFoundException, IOException {
		String creds = genMethods.getRandomUserPassPair(fileName, ",");
		String[] userPass = creds.split(",");
		mainPage.controller().navigate(SteviaContext.getParam("targetHostUrl"));
		mainPage.pressLinkLogOut();
		loginPage = mainPage.pressLinkLogin();
		loginPage.inputUsername(userPass[0]);
		loginPage.inputPassword(userPass[1]);
		loginPage.pressButtonLogin();
		mainPage.checLoggedInUsername(username);
	}
	
	@Test(alwaysRun = true, description = "input text and press the search icon, check results")
	public void testStep_1() {
		mainPage.inputSearch(search1);
		mainPage.pressSearchButton();
		mainPage.checkContentHeaderIsVisible(search1);	
	}
	
	@AfterClass(alwaysRun = true)
	public void logOut() {
		mainPage.pressLinkLogOut();
	}
}
