package assignement.atypon.searchTests;

import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.persado.oss.quality.stevia.selenium.core.SteviaContext;

import assignement.atypon.common.pageObjects.MainPageObject;
import assignement.atypon.context.TestBase;

public class BasicSearchScenarios extends TestBase {

	private static final long serialVersionUID = 1L;
@Autowired
MainPageObject mainPage;
String search1 = "Quality Assurance";
String search2 = "Test framework deployment";
String search3 = "Quality Assurance/Strategy";

	
	@BeforeClass
	public void navigateToMainPage() {
		mainPage.controller().navigate(SteviaContext.getParam("targetHostUrl"));
	}
	
	@Test(alwaysRun = true, description = "input text and press the search icon, check results")
	public void testStep_1() {
		mainPage.inputSearch(search1);
		mainPage.pressSearchButton();
		mainPage.checkContentHeaderIsVisible(search1);
	}
	
	@Test(alwaysRun = true, dependsOnMethods = "testStep_1", description = "input text and press the enter button check results")
	public void testStep_2() {
		mainPage.inputSearch(search2);
		mainPage.pressEnterToSearch();
		SteviaContext.verify().equal(mainPage.controller().getCurrentUrl().contains(search2.replace(" ", "_")), true);
	}
	
	@Test(alwaysRun = true, dependsOnMethods = "testStep_2", description = "search by using autocomplete")
	public void testStep_3() {
		mainPage.inputSearch(search3.substring(0, 5));
		mainPage.inputSearchAutocomplete(search3);
		mainPage.checkContentHeaderIsVisible(search3);	
	}
	
	@Test(alwaysRun = true, dependsOnMethods = "testStep_3", description = "search with empty string, check ")
	public void testStep_4() {
		mainPage.inputSearch("");
		mainPage.pressEnterToSearch();
		mainPage.checkMainInputFieldIsVisible();
	}
}
