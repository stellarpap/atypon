package performanceTests;

import static org.apache.jmeter.JMeter.JMETER_REPORT_OUTPUT_DIR_PROPERTY;

import java.io.File;
import java.io.FileOutputStream;

import org.apache.jmeter.config.Arguments;
import org.apache.jmeter.config.gui.ArgumentsPanel;
import org.apache.jmeter.control.LoopController;
import org.apache.jmeter.control.gui.LoopControlPanel;
import org.apache.jmeter.control.gui.TestPlanGui;
import org.apache.jmeter.engine.StandardJMeterEngine;
import org.apache.jmeter.protocol.http.control.gui.HttpTestSampleGui;
import org.apache.jmeter.protocol.http.sampler.HTTPSamplerProxy;
import org.apache.jmeter.report.dashboard.ReportGenerator;
import org.apache.jmeter.reporters.ResultCollector;
import org.apache.jmeter.reporters.Summariser;
import org.apache.jmeter.save.SaveService;
import org.apache.jmeter.testelement.TestElement;
import org.apache.jmeter.testelement.TestPlan;
import org.apache.jmeter.threads.gui.ThreadGroupGui;
import org.apache.jmeter.util.JMeterUtils;
import org.apache.jorphan.collections.HashTree;

import assignement.atypon.common.logging.GeneralLogger;

public class PerfGetMainLoginCreateAccount {

    public static void main(String[] args) throws Exception{
    	String uniqueId1 = String.valueOf(System.currentTimeMillis()).substring(5);
    	   String jmeterHome1 = System.getProperty("user.dir")+"\\lib\\apache-jmeter-4.0";
           File jmeterHome=new File(jmeterHome1);
           String slash = System.getProperty("file.separator");

           if (jmeterHome.exists()) {
               File jmeterProperties = new File(jmeterHome.getPath() + slash + "bin" + slash + "jmeter.properties");
               if (jmeterProperties.exists()) {
                   //JMeter Engine
                   StandardJMeterEngine jmeter = new StandardJMeterEngine();

                   //JMeter initialization (properties, log levels, locale, etc)
                   JMeterUtils.setJMeterHome(jmeterHome.getPath());
                   JMeterUtils.loadJMeterProperties(jmeterProperties.getPath());
                   JMeterUtils.initLogging();// you can comment this line out to see extra log messages of i.e. DEBUG level
                   JMeterUtils.initLocale();

                   // JMeter Test Plan, basically JOrphan HashTree
                   HashTree testPlanTree = new HashTree();

                   // First HTTP Sampler - open main mediawiki page
                   HTTPSamplerProxy mainPageSampler = new HTTPSamplerProxy();
                   mainPageSampler.setDomain("www.mediawiki.org/wiki/MediaWiki");
                   mainPageSampler.setPort(80);
                   mainPageSampler.setPath("/");
                   mainPageSampler.setMethod("GET");
                   mainPageSampler.setName("Open mediawiki homepage");
                   mainPageSampler.setProperty(TestElement.TEST_CLASS, HTTPSamplerProxy.class.getName());
                   mainPageSampler.setProperty(TestElement.GUI_CLASS, HttpTestSampleGui.class.getName());


                   // Second HTTP Sampler - access directly search result 'Quality_Assurance'
                   HTTPSamplerProxy createAccountSampler = new HTTPSamplerProxy();
                   createAccountSampler.setDomain("www.mediawiki.org/wiki/Quality_Assurance");
                   createAccountSampler.setPort(80);
                   createAccountSampler.setPath("/");
                   createAccountSampler.setMethod("GET");
                   createAccountSampler.setName("access directly search result 'Quality_Assurance");
                   createAccountSampler.setProperty(TestElement.TEST_CLASS, HTTPSamplerProxy.class.getName());
                   createAccountSampler.setProperty(TestElement.GUI_CLASS, HttpTestSampleGui.class.getName());

                // Third HTTP Sampler - access directly search result 'Quality_Assurance Software Testing'
                   HTTPSamplerProxy loginSampler = new HTTPSamplerProxy();
                   loginSampler.setDomain("www.mediawiki.org/wiki/Quality_Assurance#Software_testing");
                   loginSampler.setPort(80);
                   loginSampler.setPath("/");
                   loginSampler.setMethod("GET");
                   loginSampler.setName("access directly search result 'Quality_Assurance Software Testing'");
                   loginSampler.setProperty(TestElement.TEST_CLASS, HTTPSamplerProxy.class.getName());
                   loginSampler.setProperty(TestElement.GUI_CLASS, HttpTestSampleGui.class.getName());

                   // Loop Controller
                   LoopController loopController = new LoopController();
                   loopController.setLoops(10);
                   loopController.setFirst(true);
                   loopController.setProperty(TestElement.TEST_CLASS, LoopController.class.getName());
                   loopController.setProperty(TestElement.GUI_CLASS, LoopControlPanel.class.getName());
                   loopController.initialize();

                   // Thread Group
                   org.apache.jmeter.threads.ThreadGroup threadGroup = new org.apache.jmeter.threads.ThreadGroup();
                   threadGroup.setName("test media wiki");
                   threadGroup.setNumThreads(20);
                   threadGroup.setRampUp(1);
                   threadGroup.setSamplerController(loopController);
                   threadGroup.setProperty(TestElement.TEST_CLASS, ThreadGroup.class.getName());
                   threadGroup.setProperty(TestElement.GUI_CLASS, ThreadGroupGui.class.getName());

                   // Test Plan
                   TestPlan testPlan = new TestPlan("Create JMeter Script From Java Code");
                   testPlan.setProperty(TestElement.TEST_CLASS, TestPlan.class.getName());
                   testPlan.setProperty(TestElement.GUI_CLASS, TestPlanGui.class.getName());
                   testPlan.setUserDefinedVariables((Arguments) new ArgumentsPanel().createTestElement());

                   // Construct Test Plan from previously initialized elements
                   testPlanTree.add(testPlan);
                   HashTree threadGroupHashTree = testPlanTree.add(testPlan, threadGroup);
                   threadGroupHashTree.add(loginSampler);
                   threadGroupHashTree.add(createAccountSampler);
                   threadGroupHashTree.add(mainPageSampler);

                   // save generated test plan to JMeter's .jmx file format
                   SaveService.saveTree(testPlanTree, new FileOutputStream(jmeterHome + slash + "example.jmx"));

                   //add Summarizer output to get test progress in stdout like:
                   // summary =      2 in   1.3s =    1.5/s Avg:   631 Min:   290 Max:   973 Err:     0 (0.00%)
                   Summariser summer = null;
                   String summariserName = JMeterUtils.getPropDefault("summariser.name", "summary");
                   if (summariserName.length() > 0) {
                       summer = new Summariser(summariserName);
                   }


                   // Store execution results into a .jtl file
                   String uniqueFile = "example".concat(uniqueId1)+".jtl";;
                   String logFile = jmeterHome + slash + uniqueFile;
                   ResultCollector logger = new ResultCollector(summer);
                   logger.setFilename(logFile);
                   testPlanTree.add(testPlanTree.getArray()[0], logger);

                   // Run Test Plan
                   jmeter.configure(testPlanTree);
                   jmeter.run();  // https://stackoverflow.com/questions/51968942/how-to-generate-jmeter-dashboard-report-through-java-coding-not-windows-or-sh
                   JMeterUtils.setProperty(JMETER_REPORT_OUTPUT_DIR_PROPERTY, System.getProperty("user.dir")+"\\tempResults\\"+uniqueFile);
                   ReportGenerator generator = new ReportGenerator(logFile, null);
                   generator.generate();
                   GeneralLogger.info("Test completed. See " +System.getProperty("user.dir") +"\\tempResults\\"+ uniqueFile+ "\\index.html"+ " file for results");
                   System.exit(0);
               }
           }

           System.err.println("jmeter.home property is not set or pointing to incorrect location");
           System.exit(1);


       }
}
