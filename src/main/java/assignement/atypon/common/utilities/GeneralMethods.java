package assignement.atypon.common.utilities;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Random;

import com.helger.commons.collection.impl.ICommonsList;
import com.helger.commons.csv.CSVReader;

public class GeneralMethods {
	
	
	/** This method reads from a csv
	 * @param path
	 * @param seperator the type of seperator, eg "," , "."
	 * @return
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public List<String> readFromCsv(String csvFile, String seperator) {
		BufferedReader br = null;
		List<String> entries = new ArrayList<>();
		String line = "";
		try {

			br = new BufferedReader(new FileReader(csvFile));
			while ((line = br.readLine()) != null) {
				entries.add(line);
				// use comma as separator
				//                String[] country = line.split(seperator);
				//
				//                System.out.println("Country [code= " + country[4] + " , name=" + country[5] + "]");

			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return entries;
	}

	
	/** This method returns a 'random' int , between min and max
	 * @param min
	 * @param max
	 * @return
	 */
	public Integer getRandomValue(int min, int max) {
		int diff = max - min;
		Random rn = new Random();
		int i = rn.nextInt(diff + 1);
		i += min;
		return i;
	}
	
	/** This method returns a random string, from a csv file
	 * @param fileLocation
	 * @param separator
	 * @return
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public String getRandomUserPassPair(String fileLocation, String separator) throws FileNotFoundException, IOException{
		List<String> reader = readFromCsv(fileLocation, separator);
		int rand = getRandomValue(0,reader.size());
		return reader.get(rand);
	}

}
