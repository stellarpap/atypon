package assignement.atypon.common.pageObjects;

import java.text.MessageFormat;

import org.springframework.beans.factory.annotation.Autowired;

import com.persado.oss.quality.stevia.selenium.core.SteviaContext;
import com.persado.oss.quality.stevia.selenium.core.WebComponent;

import assignement.atypon.common.logging.GeneralLogger;

public class LoginPageObject extends WebComponent {

@Autowired
MainPageObject mainPage;
@Autowired
CreateAccountPageObject createAccount;
@Autowired
ResetPasswordPageObject resetPassword;

	private enum LoginPageLocators {
			INPUT_USERNAME("css=input#wpName1"),
			INPUT_PASSWORD("css=input#wpPassword1"),
			BUTTON_LOGIN("css=button#wpLoginAttempt"),
			CHECKBOX_KEEP_ME_LOGGED_IN("css=input#wpRemember"),
			LINK_HELP_LOGIN("css=a[href*='Help:Logging_in']"),
			LINK_FORGOT_PASSWORD("css=a[href*='PasswordReset']"),
			LOGIN_HEADER("css=h1.firstHeading:contains('Log in')"),
			VALIDATION_ERROR_IN_FORM("css=div#userloginForm form.mw-htmlform div.error"),
			LINK_JOIN_MEDIAWIKI("css=a#mw-createaccount-join"),
			;

		private String myLocator;

		LoginPageLocators(String locator) {
			myLocator = locator;
		}

		public String get() {
			return myLocator;
		}

		public String getWithParams(Object... params) {
			return MessageFormat.format(myLocator, params);
		}
	}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////// ACTION METHODS ///////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	/** This method inputs the username in the respective input field
	 * @param username
	 */
	public void inputUsername(String username) {
		controller().input(LoginPageLocators.INPUT_USERNAME.get(), username);
	}

	/** This method inputs the password, in the respective input field
	 * @param password  
	 */
	public void inputPassword(String password) {
		controller().input(LoginPageLocators.INPUT_PASSWORD.get(), password);
	}
	

	/** This method presses the login button
	 * 
	 */
	public MainPageObject pressButtonLogin() {
		controller().press(LoginPageLocators.BUTTON_LOGIN.get());
		return mainPage;
	}
	
	/** This methods restores username and password to defaults
	 * 
	 */
	public void clearUsernamePassword() {
		controller().executeJavascript(("$(\""+LoginPageLocators.INPUT_USERNAME.get().substring(4)+"\").val(null)"));
		controller().executeJavascript(("$(\""+LoginPageLocators.INPUT_PASSWORD.get().substring(4)+"\").val(null)"));
	}
	
	/** This method gets the html5 required validation text message
	 * @param inputField : can only take values 'username', 'password'
	 * @return the text with the validation message
	 * @throws Exception
	 */
	public String getValidationTextInputFields(String inputField) throws Exception {
		String text;
		if (inputField.equals("username")) {
			text = controller().getAttributeValue(LoginPageLocators.INPUT_USERNAME.get(), "validationMessage");
		}
		else if (inputField.equals("password")) {
			text = controller().getAttributeValue(LoginPageLocators.INPUT_PASSWORD.get(), "validationMessage");
		}
		
		else {
			GeneralLogger.error("\""+inputField+"\""+ " is not a valid option");
			throw new Exception("\""+inputField+"\""+ " is not a valid option");
		}
		return text;
	}
	
	/** This method presses the checkbox keep me logged in
	 * 
	 */
	public void pressKeepMeLoggedIn() {
		controller().executeJavascript(("$(\""+LoginPageLocators.CHECKBOX_KEEP_ME_LOGGED_IN.get().substring(4)+"\").prop('checked',true)")); // used js whereas a simple press should have been sufficient, needs investigation
	}
	/** This method checks if the "keep me logged in" checkbox is selected
	 * @return true if checked, false if unchecked
	 */
	public Boolean isKeepMeLoggedInIsChecked() {
		return (Boolean) controller().executeJavascript("var a=window.jQuery(\"" + LoginPageLocators.CHECKBOX_KEEP_ME_LOGGED_IN.get().substring(4) + "\").prop('checked'); return a;"); // used js whereas a simple verify().selected() should have been sufficient, needs investigation
	}
	
	/** This method presses the help login link
	 * 
	 */
	public void pressLinkHelpLogin() {
		controller().press(LoginPageLocators.LINK_HELP_LOGIN.get());
	}
	
	/** This method presses the forget your password link
	 * @return a resetPassword object
	 */
	public ResetPasswordPageObject pressLinkForgetYourPassword() {
		controller().press(LoginPageLocators.LINK_FORGOT_PASSWORD.get());
		return resetPassword;
	}
	
	/** This method presses the join media wiki link
	 * @return createdAccount page object
	 */
	public CreateAccountPageObject pressLinkJoinMediaWiki() {
		controller().press(LoginPageLocators.LINK_JOIN_MEDIAWIKI.get());
		return createAccount;
	}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////// VERIFICATION METHODS ///////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	/** This method checks if the login header is visible
	 *  
	 */
	public void checkLoginHeaderVisible() {
		SteviaContext.verify().elementVisible(LoginPageLocators.LOGIN_HEADER.get());
	}
	
	/** This method checks if the user input username is the expected one
	 * @param expectedText
	 */
	public void checkUsernameInput(String expectedText) {
		SteviaContext.verify().value(LoginPageLocators.INPUT_USERNAME.get(), expectedText);
	}
	
	/** This method checks if the user input password is the expected one
	 * @param expectedText
	 */
	public void checkPasswordInput(String expectedText) {
		SteviaContext.verify().value(LoginPageLocators.INPUT_PASSWORD.get(), expectedText);
	}
	
	/** This method checks that validation error class appears in login form
	 * 
	 */
	public void checkErrorInLoginFormVisible() {
		SteviaContext.verify().elementVisible(LoginPageLocators.VALIDATION_ERROR_IN_FORM.get());
	}
	
}
