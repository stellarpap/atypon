package assignement.atypon.common.pageObjects;

import java.text.MessageFormat;
import org.springframework.beans.factory.annotation.Autowired;
import com.persado.oss.quality.stevia.selenium.core.SteviaContext;
import com.persado.oss.quality.stevia.selenium.core.WebComponent;
import com.persado.oss.quality.stevia.selenium.core.controllers.commonapi.KeyInfo;

public class MainPageObject extends WebComponent {
	
	
@Autowired
CreateAccountPageObject createAccount;
@Autowired
LoginPageObject loginPage;

	public enum MainPageLocators {
			LINK_CREATE_ACCOUNT("css=#pt-createaccount a[href*='CreateAccount']"), 
			LINK_LOGIN("css=li#pt-login a[href*='UserLogin']"), 
			INPUT_SEARCH_UPPER_RIGHT("css=input#searchInput"),
			BUTTON_SEARCH("css=div#simpleSearch input#searchButton"),
			PAGE_LOGO("css=div#p-logo a.mw-wiki-logo"),
			TEXT_LOGGED_IN_USERNAME("css=div#p-personal li#pt-userpage"),
			LINK_LOG_OUT("css=div#p-personal li#pt-logout a[href*='UserLogout']"),
			SEARCH_AUTOCOMPLETE("css=div.suggestions div.suggestions-results "),
			SEARCH_AUTOCOMPLETE_SELECT("css=div.suggestions div.suggestions-results a div.suggestions-result:contains(''{0}'')"),
			CONTENT_HEADER("css=div#content h1:contains(''{0}'')"),
			INPUT_SEARCH_BAR_MAIN("css=input#ooui-php-1")
			;
		private String myLocator;

		MainPageLocators(String locator) {
			myLocator = locator;
		}

		public String get() {
			return myLocator;
		}

		public String getWithParams(Object... params) {
			return MessageFormat.format(myLocator, params);
		}
	}

	
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////// ACTION METHODS ///////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/** This method presses create account link
	 *  @return a createAccount object
	 */
	public CreateAccountPageObject pressLinkCreateAccount() {
		controller().pressAndWaitForPageToLoad(MainPageLocators.LINK_CREATE_ACCOUNT.get());
		return createAccount;
	}
	
	/** This method presses the login link
	 * @return a loginPage object
	 */
	public LoginPageObject pressLinkLogin() {
		controller().pressAndWaitForPageToLoad(MainPageLocators.LINK_LOGIN.get());
		return loginPage;
	}
	
	/** This method inputs text into search box
	 * @param input : the user text
	 */
	public void inputSearch(String input) {
		controller().input(MainPageLocators.INPUT_SEARCH_UPPER_RIGHT.get(), input);
	}
	
	/** This method presses the search bubble, in search boc
	 * 
	 */
	public void pressSearchButton() {
		controller().pressAndWaitForPageToLoad(MainPageLocators.BUTTON_SEARCH.get());
	}
	
	/** This method presses the ENTER key, in keyboard
	 * 
	 */
	public void pressEnterToSearch() {
		controller().keyPress(KeyInfo.ENTER);
		controller().waitForAjaxComplete(2000);
	}
	
	/** This methods presses the log out link. If the user is not logged in, no action is taken
	 * 
	 */
	public void pressLinkLogOut() {
		if (controller().isComponentNotVisible(MainPageLocators.LINK_LOGIN.get())){
		controller().press(MainPageLocators.LINK_LOG_OUT.get());
		}
	}
	
	public void inputSearchAutocomplete(String searchText) {
		controller().waitForElement(MainPageLocators.SEARCH_AUTOCOMPLETE.get(),8);
		controller().press(MainPageLocators.SEARCH_AUTOCOMPLETE_SELECT.getWithParams(searchText));
	}
	

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////// VERIFICATION METHODS /////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	/** This method checks that the inserted text is the text the user entered
	 * @param expectedText : the text expected to appear in the search box
	 */
	public void checkSearchBoxText(String expectedText) {
		SteviaContext.verify().text(MainPageLocators.INPUT_SEARCH_UPPER_RIGHT.get(), expectedText);
	}
	
	/** This method checks if media wiki logo is visible
	 * 
	 */
	public void checkLogoIsVisible() {
		SteviaContext.verify().elementVisible(MainPageLocators.PAGE_LOGO.get());
	}
	
	/** This method checks if the log out link is visible
	 * 
	 */
	public void checkLogOutLinkVisible() {
		SteviaContext.verify().elementVisible(MainPageLocators.LINK_LOG_OUT.get());
	}
	
	/** This method checks that the logged in user's username, appears in the appropriate section
	 * @param expectedText
	 */
	public void checLoggedInUsername(String expectedText) {
		SteviaContext.verify().text(MainPageLocators.TEXT_LOGGED_IN_USERNAME.get(), expectedText);
	}
	
	/** Check that header with expected text appears in content
	 * @param expectedText
	 */
	public void checkContentHeaderIsVisible(String expectedText) {
		SteviaContext.verify().elementVisible(MainPageLocators.CONTENT_HEADER.getWithParams(expectedText));
	}
	
	/** This method checks if a search bar is visible in main page centre
	 * 
	 */
	public void checkMainInputFieldIsVisible() {
		SteviaContext.verify().elementVisible(MainPageLocators.INPUT_SEARCH_BAR_MAIN.get());
	}

}
