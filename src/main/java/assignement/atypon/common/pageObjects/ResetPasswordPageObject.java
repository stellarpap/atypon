package assignement.atypon.common.pageObjects;

import java.text.MessageFormat;
import org.springframework.beans.factory.annotation.Autowired;

import com.persado.oss.quality.stevia.selenium.core.SteviaContext;
import com.persado.oss.quality.stevia.selenium.core.WebComponent;


public class ResetPasswordPageObject extends WebComponent {
@Autowired
CreateAccountPageObject createAccount;
@Autowired
LoginPageObject loginPage;

	public enum ResetPasswordPageLocators {
			INPUT_USERNAME("css=div#mw-input-wpUsername"),
			INPUT_PASSWORD("css=div#mw-input-wpEmail"),
			BUTTON_RESET_PASSWORD("css=button.oo-ui-buttonElement-button"),
			;
		private String myLocator;

		ResetPasswordPageLocators(String locator) {
			myLocator = locator;
		}

		public String get() {
			return myLocator;
		}

		public String getWithParams(Object... params) {
			return MessageFormat.format(myLocator, params);
		}
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////// ACTION METHODS ///////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/** This method is used to input username
	 * @param username
	 */
	public void inputUserName(String username) {
		controller().input(ResetPasswordPageLocators.INPUT_USERNAME.get(), username);
	}
	
	/** This method is used to input password
	 * @param password
	 */
	public void inputPassword(String password) {
		controller().input(ResetPasswordPageLocators.INPUT_PASSWORD.get(), password);
	}
	
	/** This method presses the reset password button
	 * 
	 */
	public void pressButtonResetPassword() {
		controller().press(ResetPasswordPageLocators.BUTTON_RESET_PASSWORD.get());
	}
	

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////// VERIFICATION METHODS /////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	/** This method checks if username input field is visible
	 * 
	 */
	public void checkUsernameFieldIsVisible() {
		SteviaContext.verify().elementVisible(ResetPasswordPageLocators.INPUT_USERNAME.get());
	}
	
	/** This method checks if password input field is visible
	 * 
	 */
	public void checkPasswordFieldIsVisible() {
		SteviaContext.verify().elementVisible(ResetPasswordPageLocators.INPUT_PASSWORD.get());
	}
	
	/** This method checks if button reset password is visible
	 * 
	 */
	public void checkButtonResetPasswordIsVisible() {
		SteviaContext.verify().elementVisible(ResetPasswordPageLocators.BUTTON_RESET_PASSWORD.get());
	}

}
