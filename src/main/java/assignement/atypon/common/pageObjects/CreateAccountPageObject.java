package assignement.atypon.common.pageObjects;

import java.text.MessageFormat;

import com.persado.oss.quality.stevia.selenium.core.SteviaContext;
import com.persado.oss.quality.stevia.selenium.core.WebComponent;
import assignement.atypon.common.logging.GeneralLogger;

public class CreateAccountPageObject extends WebComponent {

	public enum CreateAccountPageLocators{
			INPUT_USERNAME("css=input#wpName2"),
			INPUT_PASSWORD("css=input#wpPassword2"),
			INPUT_CONFIRM_PASSWORD("css=input#wpRetype"),
			INPUT_EMAIL("css=input#wpEmail"),
			INPUT_CAPTCHA("css=input#mw-input-captchaWord"),
			BUTTON_CREATE_ACCOUNT("css=button#wpCreateaccount"),
			ERROR_USERNAME_EXISTS("css=input#wpName2+span.error"),
			ERROR_IN_CAPTCHA("css=div#userloginForm div.error"),
			INPUT_FIELDS("css=input"),
;
			private String myLocator;

		private CreateAccountPageLocators(String locator) {
			myLocator = locator;
		}

		public String get() {
			return myLocator;
		}

		public String getWithParams(Object... params) {
			return MessageFormat.format(myLocator, params);
		}
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////// ACTION METHODS /////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	/** This method fills the username input field
	 * @param userName
	 */
	public void inputUsername(String userName) {
		controller().input(CreateAccountPageLocators.INPUT_USERNAME.get(), userName);
	}
	
	/** This method fills the password input field
	 * @param password
	 */
	public void inputPassword(String password) {
		controller().input(CreateAccountPageLocators.INPUT_PASSWORD.get(), password);
	}
	
	/** This method fills the email input field
	 * @param password
	 */
	public void inputEmail(String email) {
		controller().input(CreateAccountPageLocators.INPUT_EMAIL.get(), email);
	}
	
	/** This method fills the confirm password input field
	 * @param password
	 */
	public void inputConfirmPassword(String password) {
		controller().input(CreateAccountPageLocators.INPUT_CONFIRM_PASSWORD.get(), password);
	}
	
	/** This method fills the captcha input field
	 * @param text
	 */
	public void inputCaptcha(String text) {
		controller().input(CreateAccountPageLocators.INPUT_CAPTCHA.get(), text);
	}
	
	/** This method presses the submit button
	 * 
	 */
	public void pressButtonSubmit() {
		controller().press(CreateAccountPageLocators.BUTTON_CREATE_ACCOUNT.get());
	}
	
	/** This method clears all input fields in the form
	 * 
	 */
	public void cleanAllInputFields() {
		controller().executeJavascript(("$(\""+CreateAccountPageLocators.INPUT_FIELDS.get().substring(4)+"\").val(null)"));
	}
	
	/** This method gets the html5 required validation text message
	 * @param inputField : can only take values 'username', 'password', 'confirmpassword', 'captcha'
	 * @return the text with the validation message
	 * @throws Exception
	 */
	public String getValidationTextInputFields(String inputField) throws Exception {
		String text;
		if (inputField.equals("username")) {
			text = controller().getAttributeValue(CreateAccountPageLocators.INPUT_USERNAME.get(), "validationMessage");
		}
		else if (inputField.equals("password")) {
			text = controller().getAttributeValue(CreateAccountPageLocators.INPUT_PASSWORD.get(), "validationMessage");
		}
		
		else if (inputField.equals("confirmpassword")) {
			text = controller().getAttributeValue(CreateAccountPageLocators.INPUT_CONFIRM_PASSWORD.get(), "validationMessage");
		}
		
		else if (inputField.equals("captcha")) {
			text = controller().getAttributeValue(CreateAccountPageLocators.INPUT_CAPTCHA.get(), "validationMessage");
		}
		
		else {
			GeneralLogger.error("\""+inputField+"\""+ " is not a valid option");
			throw new Exception("\""+inputField+"\""+ " is not a valid option");
		}
		return text;
	}

	
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////// VERIFICATION METHODS /////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	/** Check that username field is filled correctly
	 * @param expectedUsername
	 */
	public void checkUsernameText(String expectedUsername) {
		SteviaContext.verify().value(CreateAccountPageLocators.INPUT_USERNAME.get(), expectedUsername);
	}
	
	/** Check that password field is filled correctly
	 * @param expectedUsername
	 */
	public void checkPasswordText(String expectedPassword) {
		SteviaContext.verify().value(CreateAccountPageLocators.INPUT_PASSWORD.get(), expectedPassword);
	}
	
	/** Check that username field is filled correctly
	 * @param expectedUsername
	 */
	public void checkConfirmPasswordText(String expectedPassword) {
		SteviaContext.verify().value(CreateAccountPageLocators.INPUT_CONFIRM_PASSWORD.get(), expectedPassword);
	}
	
	/** Check that email field is filled correctly
	 * @param expectedUsername
	 */
	public void checkEmailText(String email) {
		SteviaContext.verify().value(CreateAccountPageLocators.INPUT_EMAIL.get(), email);
	}
	
	/** Check that captcha field is filled correctly
	 * @param expectedUsername
	 */
	public void checkCaptchText(String captcha) {
		SteviaContext.verify().value(CreateAccountPageLocators.INPUT_CAPTCHA.get(), captcha);
	}
	
	/** Checks that submit button is visible
	 * 
	 */
	public void checkSubmitButtonVisible() {
		SteviaContext.verify().elementVisible(CreateAccountPageLocators.BUTTON_CREATE_ACCOUNT.get());
	}
	
	/** THis methods checks if the validation error for username already exists is visible
	 * 
	 */
	public void checkErrorUsernameExistsVisible() {
		controller().waitForElement(CreateAccountPageLocators.ERROR_USERNAME_EXISTS.get(), 5);
		SteviaContext.verify().elementVisible(CreateAccountPageLocators.ERROR_USERNAME_EXISTS.get());
	}
	
	public void checkCaptchaErrorVisible() {
		SteviaContext.verify().elementVisible(CreateAccountPageLocators.ERROR_IN_CAPTCHA.get());
		
	}
}
