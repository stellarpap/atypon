package assignement.atypon.common.logging;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Reporter;


public class GeneralLogger {

 
 /** The log. */
 public static Logger LOG = LoggerFactory.getLogger("GeneralLogger");
 
 /** The pass color. */
	public static String passColor = "Lime";
 
	/** The warn color. */
	public static String warnColor = "Orange";
	
 /** The fail color. */
	public static String failColor = "OrangeRed";
	
 /** The url color. */
	public static String urllColor = "Indigo";
 
/** The url color. */
	public static String actionColor = "Yellow";
		
 /**
  * Info logs into console and report log
  *
  * @param message the message
  */
 public static void info(String message) {
  System.setProperty("org.uncommons.reportng.escape-output", "false");
  LOG.info(message);
  Reporter.log("<p class=\"testOutput\" style=\"font-size:1em;\">"
    + message + "</p>");

 }

 /**
  * Warn logs into console and report log
  *
  * @param message the message
  */
 public static void warn(String message) {
  System.setProperty("org.uncommons.reportng.escape-output", "false");
  LOG.warn(message);
  Reporter.log("<p class=\"testOutput\" style=\"color:Orange; font-size:1em;\">"
    + message + "</p>");
 }

 /**
  * Error logs into console and report log
  *
  * @param message the message
  */
 public static void error(String message) {
  System.setProperty("org.uncommons.reportng.escape-output", "false");
  LOG.error(message);
  Reporter.log("<p class=\"testOutput\" style=\"color:red; font-size:1em;\">"
    + message + "</p>");
 } 
 
 /**
  * Pass logs into console and report log
  *
  * @param message the message
  */
 public static void pass(String message) {
  System.setProperty("org.uncommons.reportng.escape-output", "false");
  LOG.info(message);
  Reporter.log("<p class=\"testOutput\" style=\"color:green; font-size:1em;\">"
    + message + "</p>");
 } 
}