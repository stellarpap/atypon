package assignement.atypon.context;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.testng.*;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.xml.XmlSuite;
import org.testng.xml.XmlSuite.ParallelMode;

import com.persado.oss.quality.stevia.selenium.core.SteviaContext;
import com.persado.oss.quality.stevia.selenium.core.SteviaContextParameters;
import com.persado.oss.quality.stevia.spring.SteviaTestBase;

import assignement.atypon.common.logging.GeneralLogger;

public class TestBase extends SteviaTestBase implements SteviaContextParameters{

	private static final long serialVersionUID = 1L;
	
	private Map<String, String> testParams = new HashMap<String, String>();

	@Override
	protected void suiteInitialisation(ITestContext context) {
		configureParameters(context);
	}

	@BeforeClass(alwaysRun = true)
	protected void prepareTest(ITestContext context) {
		configureParameters(context);
		SteviaContext.registerParameters(this);
		SteviaContext.getWebController().maximizeWindow();
	}

	private void configureParameters(ITestContext context) {
		testParams = context.getSuite().getXmlSuite().getAllParameters();
		Iterator<String> it = testParams.keySet().iterator();
		while (it.hasNext()) {
			String parameter = it.next();
			if (System.getProperty(parameter) != null && !System.getProperty(parameter).isEmpty()) {
				testParams.put(parameter, System.getProperty(parameter));
			}
		}
	}
		
	public Map<String, String> getAllParameters() {
		return testParams;
	}

	public void setAllParameters(Map<String, String> params) {
		// TODO Auto-generated method stub
	}
		
	// Added to support parallel testing. But most importantly to setParameters to suite!
	@BeforeSuite(alwaysRun = true)
	@Parameters({ "parallelSetup", "threadsNumber" })
	protected void configureSuite(@Optional String parallelSetup, @Optional String threadsNumber, ITestContext context) {
		XmlSuite suite = context.getSuite().getXmlSuite();
		String suiteName = suite.getName();
		configureParameters(context);
		suite.setParameters(testParams);
		SteviaContext.registerParameters(this);
		GeneralLogger.LOG.info("Set Parallel Level of suite: " + suiteName + " to " + SteviaContext.getParam("parallelSetup"));
		if (parallelSetup.equals("tests")) {
			suite.setParallel(ParallelMode.TESTS);
		}
		if (parallelSetup.equals("classes")) {
			suite.setParallel(ParallelMode.CLASSES);
		}
		if (parallelSetup.equals("methods")) {
			suite.setParallel(ParallelMode.METHODS);
		}
		if (parallelSetup.equals(null) || parallelSetup.equals("")) {
			suite.setParallel(ParallelMode.NONE);
		}
		if (threadsNumber != null) {
			suite.setThreadCount(Integer.valueOf(threadsNumber));
		}
	}
	// added in order to re-load sizzle, if cannot be found in DOM	
	@BeforeSuite(alwaysRun = true)  
	protected void injectSizzle(){
		if(SteviaContext.getParam("browser").equals("firefox")||SteviaContext.getParam("browser").equals("chrome")){	
			System.setProperty("sizzleUrl","https://cdnjs.cloudflare.com/ajax/libs/sizzle/2.0.0/sizzle.js" );
		}	
	}
	
	// added in order to load chrome to execution path
	@BeforeSuite(alwaysRun = true) 
	protected void addChromeDriverToPath() {
		System.setProperty("webdriver.chrome.driver",  System.getProperty("user.dir")+"\\lib\\chromedriver.exe");
	}

}
